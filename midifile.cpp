#include "midifile.h"
#include "miditrack.h"
#include <QFile>
#include <QDataStream>
#include <QStringList>
#include <QDebug>
#include <QFileInfo>

//define timer interval for playing ( milliseconds )
#define INTERVAL 20
#define DEFAULT_TEMPO 500000


double MidiFile::midiFrequency[128]  = {8.1758, 8.66196, 9.17702, 9.72272, 10.3009, 10.9134, 11.5623, 12.2499, 12.9783, 13.75, 14.5676, 15.4339, 16.3516, 17.3239, 18.354, 19.4454, 20.6017, 21.8268, 23.1247, 24.4997, 25.9565, 27.5, 29.1352, 30.8677, 32.7032, 34.6478, 36.7081, 38.8909, 41.2034, 43.6535, 46.2493, 48.9994, 51.9131, 55, 58.2705, 61.7354, 65.4064, 69.2957, 73.4162, 77.7817, 82.4069, 87.3071, 92.4986, 97.9989, 103.826, 110, 116.541, 123.471, 130.813, 138.591, 146.832, 155.563, 164.814, 174.614, 184.997, 195.998, 207.652, 220, 233.082, 246.942, 261.626, 277.183, 293.665, 311.127, 329.628, 349.228, 369.994, 391.995, 415.305, 440, 466.164, 493.883, 523.251, 554.365, 587.33, 622.254, 659.255, 698.456, 739.989, 783.991, 830.609, 880, 932.328, 987.767, 1046.5, 1108.73, 1174.66, 1244.51, 1318.51, 1396.91, 1479.98, 1567.98, 1661.22, 1760, 1864.66, 1975.53, 2093, 2217.46, 2349.32, 2489.02, 2637.02, 2793.83, 2959.96, 3135.96, 3322.44, 3520, 3729.31, 3951.07, 4186.01, 4434.92, 4698.64, 4978.03, 5274.04, 5587.65, 5919.91, 6271.93, 6644.88, 7040, 7458.62, 7902.13, 8372.02, 8869.84, 9397.27, 9956.06, 10548.1, 11175.3, 11839.8, 12543.9};


MidiFile::MidiFile(QObject *parent) :
    QObject(parent), file(NULL), datastream(NULL),timePerQuarter(0),trackCount(0),currentTempo(0),songDurationMs(0),currentPlayingTimeMs(0),autoUpdateMaxSimultaneousNoteOnTrackToggleWhilePlaying(true)
{
    playTimer = new QTimer(this);
    playTimer->setInterval(INTERVAL);
    playTime = new QTime();
    connect(playTimer,SIGNAL(timeout()),this,SLOT(timerTick()));
}

void MidiFile::open(QString path){
    emit playerStateChanged(PlayerLocked);
    emit statusString("Opening");
    emit progress(0);
    close();
    file = new QFile(path);
    try{
        if(!file->open(QIODevice::ReadOnly)){
            throw QString("Can't open file");
        }
        datastream = new QDataStream(file);
        datastream->setByteOrder(QDataStream::BigEndian);
        readMidiFile();
        trackCount=trackList.size();
        calculateAndEmitHighestAndLowestNote();
        QFileInfo fileInfo(*file);
        emit updateTrackSummaryList(buildTracksSummaryList());
        emit progress(1000);
        playerControl(PlayerStop);
        emit fileOpenedInfos(fileInfo.completeBaseName(),trackCount,countMusicalTracks(), buildTimeString(songDurationMs/1000));
        emit statusString("File opened");
    }
    catch(QString const& e){  
        if(!e.isEmpty()){
            errorString=e;
        }
        else{
            errorString="Unknown Error";
        }
        emit statusString("Couldn't open file");
        emit progress(1000);
        emit playerStateChanged(PlayerClosed);
        emit fail(errorString);
    }
}

void MidiFile::readMidiFile(){
    quint32 MthdHeader;
    *datastream>>MthdHeader;
    if(MthdHeader!=0x4D546864){ //0x4D546864="MThd"= MIDI files header
        throw QString("Wrong MTHD Header");
    }

    quint32 MThdLength;
    *datastream>>MThdLength;
    if(MThdLength!=6){
        throw QString("Wrong MTHD Length");
    }

    quint16 midiFormat;
    *datastream>>midiFormat;
    if(midiFormat>1){
        throw QString("Not able to read this Midi format");
    }

    *datastream>>trackCount;

    *datastream>>timePerQuarter;
    //qDebug()<<QString("TimePerQuarter")<<QString::number(timePerQuarter);

    int percent=0;
    for(quint16 num = 0; num<trackCount; num++){
        trackList.append(new MidiTrack(datastream,num));
        percent=(int)(num*100)/trackCount;
        emit statusString("Opening : "+QString::number(percent)+"%");
        emit progress(percent*10);
    }
}

QString MidiFile::getError(void){
    return errorString;
}

void MidiFile::close(){
    playerControl(PlayerStop);
    songDurationMs=0;
    trackCount=0;
    delete datastream;
    datastream=NULL;
    delete file;
    file=NULL;
    while(!trackList.isEmpty()){
        delete trackList.last();
        trackList.removeLast();
    }
    timePerQuarter=0;
    errorString="";
    currentTempo=0;
    emit playerStateChanged(PlayerClosed);
}

int MidiFile::getTrackCount(void){
    return trackCount;
}

quint16 MidiFile::countMusicalTracks(void){
    quint16 numMusicalTracks=0;
    foreach(MidiTrack* track, trackList){
        if(track->getIsMusical()){numMusicalTracks++;}
    }
    return numMusicalTracks;
}

QList<MidiFile::TrackSummary> MidiFile::buildTracksSummaryList(void){ //USED FOR TRACK TABLE WIDGET
    QList<TrackSummary>trackSummaryList;
    TrackSummary thisTrackSummary;
    for(quint16 i =0; i<trackCount;i++){
        MidiTrack* currentTrack = trackList.at(i);
        thisTrackSummary.trackName=trackList.at(i)->getName();
        thisTrackSummary.trackNoteCount=currentTrack->getNoteCount();
        thisTrackSummary.trackMaxSimultaneousNotes=currentTrack->getMaxSimultaneousNote();
        thisTrackSummary.trackLowestNote=currentTrack->getLowestNote();
        thisTrackSummary.trackHighestNote=currentTrack->getHighestNote();
        thisTrackSummary.trackIsMusical=currentTrack->getIsMusical();
        thisTrackSummary.trackIsPlayed=currentTrack->getIsPlayed();
        trackSummaryList.append(thisTrackSummary);
    }
    return trackSummaryList;
}

QString MidiFile::buildTimeString(uint duration){//RETURN A TIME FORMATTED STRING
    QString durationString;
    quint8 time = duration%60;;
    durationString=QString::number(time)+"s";
    duration/=60;
    if(duration>0){
        time = duration%60;
        durationString.prepend(QString::number(time)+"m ");
        duration/=60;
        if(duration>0){
            durationString.prepend(QString::number(duration)+"h ");
        }
    }
    return durationString;
}

void MidiFile::calculateAndEmitHighestAndLowestNote(void){
    quint8 highestNote = 0;
    quint8 lowestNote = 127;
    foreach(MidiTrack* track,trackList){
        if(!track->getIsPlayed()){continue;}
        highestNote=qMax(highestNote,track->getHighestNote());
        lowestNote=qMin(lowestNote,track->getLowestNote());
    }
    emit lowestAndHighestNote(lowestNote,highestNote);
}

float MidiFile::calculateTickPerInterval(quint32 TickPerBeat,int interval){ //CALCULATE EQUIVALENT MIDI TICKS FOR GIVEN DURATION
    if(timePerQuarter<0){
        return ((float)(-timePerQuarter&0x7F00)*(float)(timePerQuarter&0x00FF)*(float)interval)/1000;
    }
    return (float)timePerQuarter/(float)TickPerBeat*1000*interval;
}

void MidiFile::toggleTrackPlayed(quint16 track){
    if(track<trackCount){
        if(trackList.at(track)->getIsPlayed()){
            emit stopTrack(track);
        }
        trackList.at(track)->togglePlayed();
    }
    emit updateTrackSummaryList(buildTracksSummaryList());
    calculateAndEmitHighestAndLowestNote();
    if((!playTimer->isActive())||autoUpdateMaxSimultaneousNoteOnTrackToggleWhilePlaying){
        playerControl(PlayerStop);
    }
}

void MidiFile::resetPlayer(void){
    playTimer->stop();
    currentPlayingTimeMs=0;
    currentTempo=DEFAULT_TEMPO;
    foreach(MidiTrack* track, trackList){
        track->setCurrentEventToFirst();
    }
}

void MidiFile::playerControl(PlayerState control){
    switch(control){
    case PlayerPlay:
        playTimer->start();
        playTime->start();
        emit playerStateChanged(PlayerPlay);
        emit statusString("Playing ("+buildTimeString(currentPlayingTimeMs/1000) +")");
        emit progress(1000*currentPlayingTimeMs/songDurationMs);
        break;
    case PlayerPause:
        playTimer->stop();
        emit playerStateChanged(PlayerPause);
        emit statusString("Paused ("+buildTimeString(currentPlayingTimeMs/1000) +")");
        emit progress(1000*currentPlayingTimeMs/songDurationMs);
        break;
    case PlayerStop:
        resetPlayer();
        if(trackCount>0){
            calculateSongDurationAndMaxSimultaneousNote();
            resetPlayer();
        }
        emit playerStateChanged(PlayerStop);
        emit statusString("Stopped");
        emit progress(0);
        break;
    default:
        break;
    }
}

void MidiFile::timerTick(){ //CALLED EACH TIME TIMER TIMEOUT
    int elapsedSinceLastCall=playTime->restart();
    //qDebug()<<"ELAPSED : "<<QString::number(elapsedSinceLastCall);

    //HANDLING CURRENTPLAYINGTIME
    currentPlayingTimeMs+=elapsedSinceLastCall;
    emit statusString("Playing ("+buildTimeString(currentPlayingTimeMs/1000) +")");
    emit progress(1000*currentPlayingTimeMs/songDurationMs);

    //PLAYING ALL MIDITRACKS
    QList<MidiEvent> eventList;
    MidiTrack* currentTrack=NULL;
    MidiEvent* gotEvent=NULL;
    bool allTracksEnded=true;
    for(quint16 i=0;i<trackCount;i++){
        currentTrack=trackList.at(i);
        if(currentTrack->getIsUseless()||currentTrack->getIsEnded()){continue;}
        if(allTracksEnded){allTracksEnded=false;}
        currentTrack->addToPlayTickCounter(calculateTickPerInterval(currentTempo,elapsedSinceLastCall));
        while(1){
            if(!currentTrack->getNextEvent(&gotEvent)){break;}
            if(gotEvent->isUseless()){continue;}
            if(gotEvent->getType()==MidiEvent::tempoChange){
                currentTempo=gotEvent->getParam();
                qDebug()<<"new Tempo : "<<QString::number(currentTempo);
            }
            else{
                if(currentTrack->getIsPlayed()){
                    eventToDebug(gotEvent); //for debug purposes
                    eventList.append(*gotEvent);
                }
            }
        }
    }
    emit eventsToPlayList(eventList);
    if(allTracksEnded){
        qDebug()<<"Track ended at"<<QString::number(currentPlayingTimeMs)<<", expected duration :"<<QString::number(songDurationMs);
        playerControl(PlayerStop);
    }
}

void MidiFile::calculateSongDurationAndMaxSimultaneousNote(void){ //FASTPLAY FUNCTION
    MidiTrack* currentTrack=NULL;
    MidiEvent* gotEvent=NULL;
    bool allTracksEnded=false;
    uint msCounter=0;
    uint currentSimultaneousNotes=0;
    uint maxSimultaneousNotes=0;
    float currentSongTick=calculateTickPerInterval(DEFAULT_TEMPO,INTERVAL);
    while(!allTracksEnded){
        allTracksEnded=true;
        for(quint16 i=0;i<trackCount;i++){
            currentTrack=trackList.at(i);
            if(currentTrack->getIsUseless()||currentTrack->getIsEnded()){continue;}
            if(allTracksEnded){allTracksEnded=false;}
            currentTrack->addToPlayTickCounter(currentSongTick);
            while(1){
                if(!currentTrack->getNextEvent(&gotEvent)){break;}
                if(gotEvent->isUseless()){continue;}
                if(gotEvent->getType()==MidiEvent::tempoChange){
                    currentSongTick=calculateTickPerInterval(gotEvent->getParam(),INTERVAL);
                }
                else if(currentTrack->getIsPlayed()){
                    switch(gotEvent->getType()){
                    case MidiEvent::noteOn :
                        currentSimultaneousNotes++;
                        break;
                    case MidiEvent::noteOff :
                        if(currentSimultaneousNotes>0){
                            currentSimultaneousNotes--;
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
        }
        if(currentSimultaneousNotes>maxSimultaneousNotes){maxSimultaneousNotes=currentSimultaneousNotes;}
        msCounter+=INTERVAL;
    }
    emit songMaxSimultaneousNotes(maxSimultaneousNotes);
    songDurationMs=msCounter;
}


void MidiFile::eventToDebug(MidiEvent *eventToProcess){
    quint32 eventParam=eventToProcess->getParam();
    switch(eventToProcess->getType()){
    case MidiEvent::noteOn:
        qDebug()<<"noteOn  "<<QString::number(eventParam>>8)<<QString::number(eventParam&0x000000ff);
        break;
    case MidiEvent::noteOff:
        qDebug()<<"noteOff "<<QString::number(eventParam>>8)<<QString::number(eventParam&0x000000ff);
        break;
    default:
        qDebug()<<"Wtf is doing this Event here????";
        break;
    }
}

void MidiFile::setAutoUpdateMaxSimultaneousNotes(bool autoUpdate){
    autoUpdateMaxSimultaneousNoteOnTrackToggleWhilePlaying=autoUpdate;
}

QList<bool> MidiFile::buildTrackIsPlayedList(void){
    QList<bool> toReturn;
    for(int i = 0 ;i<trackCount;i++){
        toReturn.append(trackList.at(i)->getIsPlayed());
    }
    return toReturn;
}

void MidiFile::applyTrackIsPlayedList(QList<bool> list){
    for(int i = 0;i<list.size()&&i<trackCount;i++){
        trackList.at(i)->setIsPlayed(list.at(i));
    }
    emit updateTrackSummaryList(buildTracksSummaryList());
    resetPlayer();
    calculateSongDurationAndMaxSimultaneousNote();
    playerControl(PlayerStop);
}

bool MidiFile::getAutoUpdateRequiredDrive(void){
    return autoUpdateMaxSimultaneousNoteOnTrackToggleWhilePlaying;
}

MidiFile::~MidiFile(){
    this->close();
    delete playTimer;
    delete playTime;
}

double MidiFile::midiNoteToFrequency(int note){
    if(note<0||note>127){
        return -1;
    }
    else{
        return midiFrequency[note];
    }
}
int MidiFile::frequencyToMidiNote(int freq){
    int note = 0;
    for(note=0;note<128;note++){
        if(midiFrequency[note]>=freq){break;}
    }
    return note;
}
