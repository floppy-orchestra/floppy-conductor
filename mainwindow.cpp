#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "midifile.h"
#include "gui/comportdialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QStatusBar>
#include <QProgressBar>
#include <QSerialPort>

MainWindow::MainWindow(QWidget *parent):QMainWindow(parent),ui(new Ui::MainWindow),songSettings(NULL){
    //SETTING UP WINDOW
    ui->setupUi(this);

    setUpMenuBar();
    setUpStatusBar();

    midifile=new MidiFile();
    miditocomconverter = new MidiToComConverter();
    comlink = new ComLink();
    globalSettings = new QSettings(QDir::currentPath()+"/floppyconductor.ini",QSettings::IniFormat);

    comDialog = new ComPortDialog(comlink,this);

    setUpAllConnections();

    loadGlobalSettings();

    statusBar()->showMessage("Ready");
}

void MainWindow::setUpMenuBar(void){
    //FILE MENU
    quitAction = new QAction("Quit",this);
    connect(quitAction,SIGNAL(triggered()),qApp,SLOT(quit()));
    saveSongSettingsAction = new QAction("Save song related settings",this);
    connect(saveSongSettingsAction,SIGNAL(triggered()),this,SLOT(saveSongSettings()));
    fileMenu = menuBar()->addMenu("File");
    fileMenu->addAction(saveSongSettingsAction);
    fileMenu->addSeparator();
    fileMenu->addAction(quitAction);

    //SETTINGS MENU
    resetTrackTableSectionsSizesAction = new QAction("Auto adjust track table columns",this);
    connect(resetTrackTableSectionsSizesAction,SIGNAL(triggered()),ui->trackTable,SLOT(autoAdaptAllSectionsSize()));
    optionMenu = menuBar()->addMenu(("Settings"));
    optionMenu->addAction(resetTrackTableSectionsSizesAction);

    //HELP MENU
    aboutAction = new QAction("About",this);
    connect(aboutAction,SIGNAL(triggered()),SLOT(showAboutwindow()));
    aboutQtAction = new QAction("About Qt",this);
    connect(aboutQtAction, SIGNAL(triggered(bool)),qApp,SLOT(aboutQt()));
    helpMenu = menuBar()->addMenu("?");
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(aboutQtAction);
}

void MainWindow::setUpStatusBar(void){
    QStatusBar* tempStatusBar=this->statusBar();
    statusProgressBar = new QProgressBar();
    statusProgressBar->setTextVisible(false);
    statusProgressBar->setRange(0,1000);
    statusProgressBar->setFixedHeight(20);
    statusModeDisplay = new ModeDisplayWidget();
    tempStatusBar->addPermanentWidget(statusModeDisplay);
    tempStatusBar->addPermanentWidget(statusProgressBar);
}

void MainWindow::setUpAllConnections(void){
    //CONNECT THIS<->MIDIFILE
    connect(midifile,&MidiFile::fail,this,&MainWindow::midiOpenFail);
    connect(midifile,&MidiFile::progress,this,&MainWindow::setStatusProgressBar);
    connect(midifile,&MidiFile::statusString,this,&MainWindow::setStatusString);
    connect(midifile,&MidiFile::playerStateChanged,this,&MainWindow::setStatusModeDisplay);
    connect(midifile,&MidiFile::fileOpenedInfos,this,&MainWindow::loadSongSettings);

    //CONNECT CONTROLWIDGET<->MIDIFILE
    connect(ui->controlWidget,&playControlWidget::open,midifile,&MidiFile::open);
    connect(midifile,&MidiFile::playerStateChanged,ui->controlWidget,&playControlWidget::newStatus);
    connect(ui->controlWidget,&playControlWidget::control,midifile,&MidiFile::playerControl);
    connect(midifile,&MidiFile::fileOpenedInfos,ui->controlWidget,&playControlWidget::updateFileInfo);

    //CONNECT TRACKTABLE<->MIDIFILE
    connect(midifile,&MidiFile::updateTrackSummaryList,ui->trackTable,&TrackTableWidget::updateTrackTable);
    connect(ui->trackTable,&TrackTableWidget::toggleTrackPlayed,midifile,&MidiFile::toggleTrackPlayed);

    //CONNECT TRANSPOSECONTROL<->MIDIFILE
    connect(midifile,&MidiFile::lowestAndHighestNote,ui->transposeControl,&TransposeControlWidget::originalHighestAndLowestNote);

    //CONNECT DRIVEMANAGEMENT<->MIDIFILE
    connect(midifile,&MidiFile::songMaxSimultaneousNotes,ui->drivesManagement,&DrivesManagementWidget::neededDrivesNumberChanged);
    connect(ui->drivesManagement,&DrivesManagementWidget::autoUpdateNeededDrivesWhilePlaying,midifile,&MidiFile::setAutoUpdateMaxSimultaneousNotes);

    //CONNECT MIDIFILE<->MIDITOCOMCONVERTER
    connect(midifile,&MidiFile::eventsToPlayList,miditocomconverter,&MidiToComConverter::eventListToConvert);
    connect(midifile,&MidiFile::playerStateChanged,miditocomconverter,&MidiToComConverter::onPlayerStateChanged);
    connect(midifile,&MidiFile::stopTrack,miditocomconverter,&MidiToComConverter::stopTrack);

    //CONNECT MIDITOCOMCONVERTER<->TRANSPOSECONTROL
    connect(ui->transposeControl,&TransposeControlWidget::transposeCommand,miditocomconverter,&MidiToComConverter::transposeCommand);

    //CONNECT MIDITOCOMCONVERTER<->DRIVEMANAGEMENT
    connect(ui->drivesManagement,&DrivesManagementWidget::availableDrivesNumberChanged,miditocomconverter,&MidiToComConverter::setAvailableDrivesNum);

    //CONNECT MIDITOCOMCONVERTER<->TRACKTABLEWIDGET
    connect(miditocomconverter,&MidiToComConverter::driveMapUpdated,ui->trackTable,&TrackTableWidget::updateDriveMap);
    connect(ui->trackTable,&TrackTableWidget::driveMappingEditedByUser,miditocomconverter,&MidiToComConverter::setDriveMapping);

    //CONNECT MIDITOCOMCONVERTER<->COMLINK
    connect(miditocomconverter,&MidiToComConverter::comData,comlink,&ComLink::writeByteArray);

    //CONNECT LINKSTATUSWIDGET<->COMLINK
    connect(ui->linkStatus,&linkStatusWidget::toggleLinkState,comlink,&ComLink::togglePortState);
    connect(comlink,&ComLink::portStateChanged,ui->linkStatus,&linkStatusWidget::updatePortStatus);
    connect(comlink,&ComLink::portSettingsChanged,ui->linkStatus,&linkStatusWidget::updateSelectedPort);
    connect(comlink,&ComLink::statusString,ui->linkStatus,&linkStatusWidget::updateStatusString);

    //CONNECT LINKSTATUSWIDGET<->COMPORTDIALOG
    connect(ui->linkStatus,&linkStatusWidget::comPortButtonClicked,comDialog,&ComPortDialog::exec);
}

void MainWindow::midiOpenFail(QString error){
    QMessageBox msgBox;
    msgBox.setText("Error : "+error);
    msgBox.exec();
}

void MainWindow::setStatusString(QString status){
    statusBar()->showMessage(status);
}

void MainWindow::setStatusProgressBar(int value){
    statusProgressBar->setValue(value);
}

void MainWindow::setStatusModeDisplay(MidiFile::PlayerState state){
    statusModeDisplay->setMode(state);
}

void MainWindow::showAboutwindow(void){
    AboutDialog aboutDialog;
    aboutDialog.exec();
}

void MainWindow::loadSongSettings(QString fileName){
    delete songSettings;
    songSettings = new QSettings(QDir::currentPath()+"/songs_settings/"+fileName+".flop",QSettings::IniFormat);

    QList<bool> trackIsPlayedList;
    QStringList driveMapStringList;
    bool noMapperSettings = true;
    int arraySize = songSettings->beginReadArray("Tracks");
    for (int i = 0; i < arraySize; ++i) {
        songSettings->setArrayIndex(i);
        trackIsPlayedList.append(songSettings->value("is_played").toBool());
        if(songSettings->contains("drive_map")){
            if(noMapperSettings){noMapperSettings=false;}
        }
        driveMapStringList.append(songSettings->value("drive_map").toString());
    }
    songSettings->endArray();
    midifile->applyTrackIsPlayedList(trackIsPlayedList);

    miditocomconverter->loadDriveMapFromSettings(driveMapStringList);

    if(songSettings->contains("Transposer/value")){
        ui->transposeControl->transposeSetting(songSettings->value("Transposer/value").toInt());
    }
}

void MainWindow::saveSongSettings(void){
    if(songSettings!=NULL){
        songSettings->setValue("Transposer/value",miditocomconverter->getTransposeValue());
        QList<bool> trackIsPlayedList = midifile->buildTrackIsPlayedList();
        QMap<int,QString> driveStringMap = miditocomconverter->getDriveStringMap();
        songSettings->beginWriteArray("Tracks");
        for (int i = 0; i < trackIsPlayedList.size(); ++i) {
            songSettings->setArrayIndex(i);
            songSettings->setValue("is_played", trackIsPlayedList.at(i));
            songSettings->setValue("drive_map",(driveStringMap.contains(i) ?
                                                    driveStringMap.value(i) :
                                                    QString()));
        }
        songSettings->endArray();
        statusBar()->showMessage("Song related settings saved",2000);
    }
}

void MainWindow::loadGlobalSettings(void){
    ComLink::SerialSettings serialSettings = comlink->getSettings();
    if(globalSettings->contains("Link/port_name"))
        serialSettings.portName=globalSettings->value("Link/port_name").toString();

    if(globalSettings->contains("Link/baud_rate"))
        serialSettings.baudRate=globalSettings->value("Link/baud_rate").toInt();

    if(globalSettings->contains("Link/data_bits"))
        serialSettings.dataBits=(QSerialPort::DataBits)globalSettings->value("Link/data_bits").toInt();

    if(globalSettings->contains("Link/parity"))
        serialSettings.parity=(QSerialPort::Parity)globalSettings->value("Link/parity").toInt();

    if(globalSettings->contains("Link/stop_bits"))
        serialSettings.stopBits=(QSerialPort::StopBits)globalSettings->value("Link/stop_bits").toInt();

    if(globalSettings->contains("Link/flow_control"))
        serialSettings.flowControl=(QSerialPort::FlowControl)globalSettings->value("Link/flow_control").toInt();

    comlink->setSettings(serialSettings);

    if(globalSettings->contains("Drives/auto_update_required_drives"))
        ui->drivesManagement->setAutoUpdate(globalSettings->value("Drives/auto_update_required_drives").toBool());

    if(globalSettings->contains("Drives/available_drives"))
        ui->drivesManagement->setAvailableDrivesNumber(globalSettings->value("Drives/available_drives").toInt());

    if(globalSettings->contains("Transposer/drives_cut_off_frequency"))
        ui->transposeControl->setCutOffFrequency(globalSettings->value("Transposer/drives_cut_off_frequency").toInt());

    if(globalSettings->contains("Transposer/auto_transpose"))
        ui->transposeControl->setAutoChecked(globalSettings->value("Transposer/auto_transpose").toBool());

    if(globalSettings->contains("File/last_opened_file"))
        ui->controlWidget->setLastOpenedFilePath(globalSettings->value("File/last_opened_file").toString());

}

void MainWindow::saveGlobalSettings(void){
    ComLink::SerialSettings serialSettings = comlink->getSettings();
    globalSettings->setValue("Link/port_name",serialSettings.portName);
    globalSettings->setValue("Link/baud_rate",serialSettings.baudRate);
    globalSettings->setValue("Link/data_bits",serialSettings.dataBits);
    globalSettings->setValue("Link/parity",serialSettings.parity);
    globalSettings->setValue("Link/stop_bits",serialSettings.stopBits);
    globalSettings->setValue("Link/flow_control",serialSettings.flowControl);

    globalSettings->setValue("Drives/auto_update_required_drives",midifile->getAutoUpdateRequiredDrive());
    globalSettings->setValue("Drives/available_drives",ui->drivesManagement->getAvailableDrivesNumber());
    globalSettings->setValue("Transposer/drives_cut_off_frequency",ui->transposeControl->getCutOffFrequency());
    globalSettings->setValue("Transposer/auto_transpose",ui->transposeControl->getAutoChecked());
    globalSettings->setValue("File/last_opened_file",ui->controlWidget->getLastOpenedFilePath());
}

void MainWindow::closeEvent(QCloseEvent *event){
    Q_UNUSED(event);
    midifile->close();
    saveGlobalSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete midifile;
    delete comlink;
    delete miditocomconverter;
    delete globalSettings;
    delete songSettings;
}
