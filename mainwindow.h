#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QStatusBar>
#include <QMainWindow>
#include <QProgressBar>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QSettings>
#include "midifile.h"
#include "comlink.h"
#include "miditocomconverter.h"
#include "gui/aboutdialog.h"
#include "gui/modedisplaywidget.h"
#include "gui/comportdialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots :

private slots:
    void midiOpenFail(QString error);
    void setStatusProgressBar(int value);
    void setStatusString(QString status);
    void setStatusModeDisplay(MidiFile::PlayerState state);
    void showAboutwindow(void);
    void loadSongSettings(QString fileName);
    void saveSongSettings(void);

signals:
    
private:
    Ui::MainWindow *ui;
    MidiFile *midifile;
    ComLink* comlink;
    MidiToComConverter* miditocomconverter;

    ComPortDialog* comDialog;

    QSettings* globalSettings;
    QSettings* songSettings;
    //MENU BAR
    QAction* quitAction;
    QAction* saveSongSettingsAction;
    QAction* aboutAction;
    QAction* aboutQtAction;
    QAction* resetTrackTableSectionsSizesAction;
    QMenu* fileMenu;
    QMenu* optionMenu;
    QMenu* helpMenu;

    //STATUS BAR
    QProgressBar* statusProgressBar;
    ModeDisplayWidget* statusModeDisplay;

    void setUpMenuBar(void);
    void setUpStatusBar(void);
    void setUpAllConnections(void);
    void closeEvent(QCloseEvent *event);
    void loadGlobalSettings(void);
    void saveGlobalSettings(void);
};

#endif // MAINWINDOW_H
