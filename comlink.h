#ifndef COMLINK_H
#define COMLINK_H

#include <QObject>
#include <QSerialPort>

class ComLink : public QObject
{
    Q_OBJECT
public:
    struct SerialSettings {
        QString portName;
        qint32 baudRate;
        QSerialPort::DataBits dataBits;
        QSerialPort::Parity parity;
        QSerialPort::StopBits stopBits;
        QSerialPort::FlowControl flowControl;
    };

    explicit ComLink(QObject *parent = 0);
    SerialSettings getSettings();
    
signals:
    void portStateChanged(bool isOpen);
    void portSettingsChanged(ComLink::SerialSettings settings);
    void statusString(QString status);
    
public slots:
    void setSettings(ComLink::SerialSettings settings);
    void togglePortState(void);
    void writeByteArray(QByteArray dataToWrite);

private slots:
    void handleError(QSerialPort::SerialPortError error);

private:
    QString QByteArrayToQString(QByteArray byteArray);

    QSerialPort* port;
};

#endif // COMLINK_H
