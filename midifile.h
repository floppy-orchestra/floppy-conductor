#ifndef MIDIFILE_H
#define MIDIFILE_H

#include <QObject>
#include <QFile>
#include <QMultiMap>
#include <QTimer>
#include <QTime>
#include <QList>
#include "miditrack.h"

class MidiFile : public QObject
{
    Q_OBJECT
public:
    enum PlayerState{PlayerPlay, PlayerPause, PlayerStop, PlayerLocked, PlayerClosed};

    struct TrackSummary{
        QString trackName;
        int trackNoteCount;
        int trackMaxSimultaneousNotes;
        quint8 trackHighestNote;
        quint8 trackLowestNote;
        bool trackIsMusical;
        bool trackIsPlayed;
    };
    explicit MidiFile(QObject *parent = 0);
    QString getError(void);
    int getTrackCount(void);
    QList<bool> buildTrackIsPlayedList(void);
    void applyTrackIsPlayedList(QList<bool> list);
    bool getAutoUpdateRequiredDrive(void);
    ~MidiFile();

    static double midiNoteToFrequency(int note);
    static int frequencyToMidiNote(int freq);
    
signals:
    void fileOpenedInfos(QString trackName, quint16 trackCount, quint16 numMusicalTracks, QString duration);
    void updateTrackSummaryList(QList<MidiFile::TrackSummary> trackSummaryList);
    void lowestAndHighestNote(quint8 lowestNote, quint8 highestNote);

    void playerStateChanged(MidiFile::PlayerState status);

    void progress(int percent);
    void statusString(QString status);
    void fail(QString error);

    void songMaxSimultaneousNotes(uint simultaneousNotes);
    void eventsToPlayList(QList<MidiEvent> list);
    void stopTrack(quint16 track);

public slots:
    void open(QString path);
    void close();
    void playerControl(MidiFile::PlayerState control);
    void toggleTrackPlayed(quint16 track);
    void setAutoUpdateMaxSimultaneousNotes(bool autoUpdate);

private slots:
    void timerTick();

private :
    static double midiFrequency[128];
    QFile *file;
    QDataStream *datastream;
    QString errorString;
    qint16 timePerQuarter;
    quint16 trackCount;
    QList<MidiTrack*> trackList;
    QTimer* playTimer;
    QTime* playTime;
    quint32 currentTempo;
    uint songDurationMs;
    uint currentPlayingTimeMs;//used for currentPlayingTime management
    bool autoUpdateMaxSimultaneousNoteOnTrackToggleWhilePlaying;

    QList<TrackSummary> buildTracksSummaryList(void);
    void readMidiFile();
    float calculateTickPerInterval(quint32 TickPerBeat, int interval);
    quint16 countMusicalTracks(void);
    void resetPlayer(void);
    void eventToDebug(MidiEvent* eventToProcess);
    void calculateSongDurationAndMaxSimultaneousNote(void);
    QString buildTimeString(uint duration);
    void calculateAndEmitHighestAndLowestNote(void);
};

#endif // MIDIFILE_H
