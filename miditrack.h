#ifndef MIDITRACK_H
#define MIDITRACK_H
#include "midievent.h"
#include <QObject>


class MidiFile;
class MidiTrack
{
public:
    MidiTrack(QDataStream *datastream, const quint16 trackNum);
    ~MidiTrack();

    QString getName(void);
    bool getIsMusical(void);
    bool getIsUseless(void);
    int getNoteCount(void);
    quint8 getHighestNote(void);
    quint8 getLowestNote(void);
    int getMaxSimultaneousNote(void);

    bool getIsPlayed(void);
    void setIsPlayed(bool nextIsPlayed);
    void togglePlayed(void);

    void setCurrentEventToFirst(void);

    //Return false if currentEvent is last event;
    bool getNextEvent(MidiEvent **gotEvent);

    void addToPlayTickCounter(float valueToAdd);

    bool getIsEnded(void);

private:
    quint32 readVariableLength(QDataStream* datastream, quint32* byteRead);
    void readEvents(QDataStream* datastream, const quint16 trackNum);


    MidiEvent* firstEvent;
    MidiEvent* currentEvent;

    QString trackName;
    bool isMusical;
    bool isUseless;
    int noteCount;
    int maxSimultaneousNotes;
    quint8 highestNote;
    quint8 lowestNote;
    bool isPlayed;

    float playTickCounter;
};

#endif // MIDITRACK_H
