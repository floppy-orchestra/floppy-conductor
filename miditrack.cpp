#include "miditrack.h"
#include "midievent.h"
#include <QObject>
#include <QDataStream>
#include <QDebug>

class MidiFile;

MidiTrack::MidiTrack(QDataStream *datastream, const quint16 trackNum):firstEvent(NULL), currentEvent(NULL),isMusical(false),isUseless(true),noteCount(0),maxSimultaneousNotes(0),highestNote(0),lowestNote(127),isPlayed(false),playTickCounter(0)
{
    quint32 MtrkHeader;
    *datastream>>MtrkHeader;
    if(MtrkHeader!=0x4D54726B){ //0x4D54726B="MTrk"= MIDI track header
        throw QString("Wrong MTRK Header in track "+QString::number(trackNum));
    }
    firstEvent = new MidiEvent(trackNum, MidiEvent::beginTrack);
    setCurrentEventToFirst();
    readEvents(datastream, trackNum);
}

void MidiTrack::readEvents(QDataStream *datastream, const quint16 trackNum){
    quint32 byteRead=0;
    quint8 eventType,prevEventType;
    quint8 firstParam,dummy8;
    quint32 byteCount;
    quint32 paramLength;
    int currentSimultaneousNotes=0;

    quint32 trackLength;
    *datastream>>trackLength;

    MidiEvent* newEvent=NULL;
    while(1){
        quint32 eventDelay = readVariableLength(datastream, &byteRead);
        prevEventType=eventType;
        byteRead++; *datastream>>eventType;

        if(eventType<0xF0){//MIDI CHANNEL EVENT
            if(eventType<0x80){//RUNNING STATUS : same event type as previous event. In this case, eventType represent the first parameter
                firstParam=eventType;
                eventType=prevEventType;
            }
            else{
                byteRead++; *datastream>>firstParam;
            }


            switch (eventType&0xF0){
                case 0x80: //NOTE OFF
                    byteRead++; *datastream>>dummy8;
                    newEvent=new MidiEvent(trackNum, eventDelay,MidiEvent::noteOff,(((quint32)(eventType&0x0F))<<8)|(quint32)firstParam);
                break;

                case 0x90: //NOTE ON
                    byteRead++; *datastream>>dummy8;
                    if(dummy8==0){  //VELOCITY = 0, weird way sometimes used to turn off notes
                        newEvent=new MidiEvent(trackNum, eventDelay,MidiEvent::noteOff,(((quint32)(eventType&0x0F))<<8)|(quint32)firstParam);
                    }
                    else{
                        newEvent=new MidiEvent(trackNum, eventDelay,MidiEvent::noteOn,(((quint32)(eventType&0x0F))<<8)|(quint32)firstParam);
                    }
                break;

                case 0xA0: //AFTERTOUCH
                    byteRead++; *datastream>>dummy8;
                    newEvent=new MidiEvent(trackNum, eventDelay);
                break;

                case 0xB0: //CONTROLLER
                    byteRead++; *datastream>>dummy8;
                    newEvent=new MidiEvent(trackNum, eventDelay);
                break;

                case 0xC0: //PROGRAM CHANGE
                    newEvent=new MidiEvent(trackNum, eventDelay);
                break;

                case 0xD0: //CHANNEL AFTERTOUCH
                    newEvent=new MidiEvent(trackNum, eventDelay);
                break;

                case 0xE0: //PITCH BEND
                    byteRead++; *datastream>>dummy8;
                    newEvent=new MidiEvent(trackNum, eventDelay);
                break;

                default:
                    throw QString("Wrong Channel Event type (%3) in track : %1 at byte %2").arg(QString::number(trackNum),QString::number(byteRead),QString::number(eventType,16));
                break;
            }
        }
        else if(eventType==0xFF){//META EVENT

            byteRead++; *datastream>>eventType;

            paramLength=readVariableLength(datastream, &byteRead);

            if(eventType==0x03){//TRACK NAME
                quint8 readChar;
                for(byteCount=0;byteCount<paramLength;byteCount++){
                    byteRead++; *datastream>>readChar;
                    trackName.append((char)readChar);
                }
            }
            else if(eventType==0x51){//TEMPO CHANGE
                quint8 tempo8;
                quint32 tempo32=0;
                for(byteCount=0;byteCount<paramLength;byteCount++){
                    byteRead++; *datastream>>tempo8;
                    tempo32<<=8;
                    tempo32|=tempo8;
                }
                newEvent=new MidiEvent(trackNum, 0,MidiEvent::tempoChange,tempo32);
            }
            else if(eventType==0x2F){//END OF TRACK
                newEvent=new MidiEvent(trackNum, MidiEvent::endOfTrack);
            }
            else{
                for(byteCount=0;byteCount<paramLength;byteCount++){
                    byteRead++; *datastream>>dummy8;
                }
            }
        }
        else{   //SYSTEM EXCLUSIVE EVENT
            paramLength=readVariableLength(datastream, &byteRead);
            for(byteCount=0;byteCount<paramLength;byteCount++){
                byteRead++; *datastream>>dummy8;
            }
        }

        //SET EVENTS LINKING
        if(currentEvent!=newEvent&&newEvent!=NULL){

            currentEvent->setNextEvent(newEvent);
            currentEvent=newEvent;

            //OPERATE COUNTERS & HIGHEST / LOWEST NOTE
            if(currentEvent->getType()==MidiEvent::noteOn){
                noteCount++;
                currentSimultaneousNotes++;
                highestNote=qMax(firstParam,highestNote);
                lowestNote=qMin(firstParam,lowestNote);
            }
            else if(currentEvent->getType()==MidiEvent::noteOff) {
                currentSimultaneousNotes--;
            }

            maxSimultaneousNotes=qMax(currentSimultaneousNotes, maxSimultaneousNotes);

            //SET FLAGS
            if(!isMusical&&currentEvent->isMusical()){isMusical=true;}
            if(isUseless&&!currentEvent->isUseless()){isUseless=false;}

            //LEAVE THIS LOOP IF LAST EVENT
            if(currentEvent->getType()==MidiEvent::endOfTrack){
                setCurrentEventToFirst();
                break;
            }
        }

        if(byteRead>trackLength){//IF BYTE MISSED
            throw QString("Track "+QString::number(trackNum)+" ends at position "+QString::number(byteRead+0x3C)+" but no EOT found");
        }
    }
    isPlayed=!isUseless;
    if(trackName.isEmpty())trackName="No Title";
}

//READ MIDI "VARIABLE LENGTH" VALUES
quint32 MidiTrack::readVariableLength(QDataStream* datastream, quint32* byteRead){
    quint32 VariableLength = 0;
    quint8 byte = 0;
    do {
        (*byteRead)++;  *datastream>>byte;
        VariableLength<<=7;
        VariableLength|=(byte&0x7F);
    } while (byte & 0x80);
    return VariableLength;
}

QString MidiTrack::getName(void){
    return trackName;
}

bool MidiTrack::getIsMusical(void){
    return isMusical;
}

bool MidiTrack::getIsUseless(void){
    return isUseless;
}

int MidiTrack::getNoteCount(void){
    return noteCount;
}

int MidiTrack::getMaxSimultaneousNote(void){
    return maxSimultaneousNotes;
}

quint8 MidiTrack::getHighestNote(void){
    return highestNote;
}

quint8 MidiTrack::getLowestNote(void){
    return lowestNote;
}

bool MidiTrack::getIsPlayed(void){
    return isPlayed;
}

void MidiTrack::togglePlayed(void){
    if(isMusical){
        isPlayed=!isPlayed;
    }
}

void MidiTrack::setIsPlayed(bool nextIsPlayed){
    if(isMusical){
        isPlayed=nextIsPlayed;
    }
}

void MidiTrack::setCurrentEventToFirst(void){
    currentEvent=firstEvent;
    playTickCounter=0;
}

bool MidiTrack::getIsEnded(void){
    if(currentEvent->getType()==MidiEvent::endOfTrack){
        return true;
    }
    else{
        return false;
    }
}

void MidiTrack::addToPlayTickCounter(float valueToAdd){
    playTickCounter+=valueToAdd;
}

bool MidiTrack::getNextEvent(MidiEvent** gotEvent){ //GOTEVENT = &NEXT EVENT IF ITS DELTATIME<PLAYTICKCOUNTER, ELSE RETURN CURRENT EVENT
    if(currentEvent->getType()==MidiEvent::endOfTrack){return false;}
    MidiEvent* nextEvent = currentEvent->getNextEvent();
    float deltaNextEvent=nextEvent->getDeltaTime();
    if(deltaNextEvent<=playTickCounter){
        playTickCounter-=deltaNextEvent;
        currentEvent=nextEvent;
        *gotEvent=currentEvent;
        return true;
    }
    else{
        *gotEvent=currentEvent;
        return false;
    }
}

MidiTrack::~MidiTrack(){
    MidiEvent* nextEvent=NULL;
    MidiEvent* previousEvent=firstEvent;
    while(previousEvent->getType()!=MidiEvent::endOfTrack){
        nextEvent=previousEvent->getNextEvent();
        delete previousEvent;
        previousEvent=nextEvent;
    }
    delete previousEvent;
}
