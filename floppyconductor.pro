QT       += core gui widgets serialport

HEADERS += \
    miditrack.h \
    midifile.h \
    midievent.h \
    mainwindow.h \
    gui/transposecontrolwidget.h \
    gui/tracktablewidget.h \
    gui/playcontrolwidget.h \
    gui/modedisplaywidget.h \
    gui/linkstatuswidget.h \
    gui/aboutdialog.h \
    comlink.h \
    miditocomconverter.h \
    gui/drivesmanagementwidget.h \
    gui/comportdialog.h

SOURCES += \
    miditrack.cpp \
    midifile.cpp \
    midievent.cpp \
    mainwindow.cpp \
    main.cpp \
    gui/transposecontrolwidget.cpp \
    gui/tracktablewidget.cpp \
    gui/playcontrolwidget.cpp \
    gui/modedisplaywidget.cpp \
    gui/linkstatuswidget.cpp \
    gui/aboutdialog.cpp \
    comlink.cpp \
    miditocomconverter.cpp \
    gui/drivesmanagementwidget.cpp \
    gui/comportdialog.cpp

FORMS += \
    mainwindow.ui \
    gui/aboutdialog.ui \
    gui/comportdialog.ui

RESOURCES += \
    resources.qrc

DEFINES += VERSION=\\\"1.2.1\\\"

win32 {
DEFINES += BUILDDATE=\\\"$$system('echo %date%')\\\"
} else {
DEFINES += BUILDDATE=\\\"$$system(date '+%d/%m/%y')\\\"
}

RC_FILE = app.rc


