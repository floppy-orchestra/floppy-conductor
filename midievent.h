#ifndef MIDIEVENT_H
#define MIDIEVENT_H
#include <QObject>



class MidiEvent
{
public:
    enum MidiEventType{tempoChange,noteOn, noteOff, endOfTrack, beginTrack, other};

    MidiEvent(quint16 newTrackNum);
    MidiEvent(quint16 newTrackNum,quint32 newDeltaTime);
    MidiEvent(quint16 newTrackNum,quint32 newDeltaTime,MidiEventType newType, quint32 newParam);
    MidiEvent(quint16 newTrackNum,MidiEventType newType);

    quint32 getDeltaTime(void);
    MidiEventType getType(void);
    quint32 getParam(void);
    MidiEvent* getNextEvent(void);
    quint16 getTrackNum(void);

    bool isMusical(void);
    bool isUseless(void);

    void setNextEvent(MidiEvent* newNextEvent);

private:
    MidiEvent *nextEvent;
    quint32 deltaTime;
    quint32 param;
    quint16 trackNum;
    MidiEvent::MidiEventType type;
};

#endif // MIDIEVENT_H
