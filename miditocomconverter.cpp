#include "miditocomconverter.h"
#include <iostream>

DummyFloppyDrive::DummyFloppyDrive(int newDriveNum):isPlaying(false),isPaused(false),originalNote(0),transposedNote(0)
{
    driveNum=newDriveNum;

#ifdef GENERATE_NOTE_ARRAY
    noteTime = new QTime();
#endif
}

DummyFloppyDrive::~DummyFloppyDrive(){
#ifdef GENERATE_NOTE_ARRAY
    delete noteTime;
#endif
}

int DummyFloppyDrive::getDriveNum(void){
    return driveNum;
}

bool DummyFloppyDrive::getIsPlaying(void){
    return isPlaying;
}

uint DummyFloppyDrive::getOriginalNote(void){
    return originalNote;
}

uint DummyFloppyDrive::getTransposedNote(void){
    return transposedNote;
}

void DummyFloppyDrive::updateTransposedNote(int transposeValue){
    transposedNote=originalNote+12*transposeValue;
    if(transposedNote>127)transposedNote=127;
    if(isPlaying && !isPaused)
        sendOnCommand();
}

void DummyFloppyDrive::play(uint newOriginalNote, int transposeValue){
 #ifdef GENERATE_NOTE_ARRAY
    appendReport();
#endif
    originalNote=newOriginalNote;
    isPlaying=true;
    updateTransposedNote(transposeValue);
}

void DummyFloppyDrive::resume(){
    if(!isPaused) return;
    isPaused=false;
    if(isPlaying)
        sendOnCommand();

}

void DummyFloppyDrive::pause(){
    if(isPaused) return;
    isPaused=true;
    if(isPlaying)
        sendOffCommand();

}

void DummyFloppyDrive::stop(void){
#ifdef GENERATE_NOTE_ARRAY
   appendReport();
#endif
    sendOffCommand();
    isPlaying=false;
    isPaused=false;
    originalNote=0;
    transposedNote=0;
}

void DummyFloppyDrive::sendOnCommand(){
    //send driveOn byte array
    QByteArray ba;
    ba.resize(2);
    ba[0]=(char)transposedNote;
    ba[1]=(char)(0xC0+driveNum);
    emit comData(ba);
}

void DummyFloppyDrive::sendOffCommand(){
    //send driveOff byte array
    QByteArray ba;
    ba.resize(1);
    ba[0]=(char)(0x80+driveNum);
    emit comData(ba);
}

#ifdef GENERATE_NOTE_ARRAY

void DummyFloppyDrive::appendReport(void){
    noteReport n;
    n.msDuration = noteTime->restart();
    n.note = originalNote;
    if((n.note != 0)||(n.msDuration != 0))
        noteList.append(n);
}

void DummyFloppyDrive::printNoteReport(void){
    std::cout <<"noteArray"<< driveNum <<" = {";
    while(!noteList.isEmpty()){
        noteReport n = noteList.takeFirst();
        std::cout << "{" << n.msDuration << "," << n.note << "}";
        if(!noteList.isEmpty())
            std::cout << ",";
    }
    std::cout << "};\n";
    std::flush(std::cout);
}

#endif


//**************************************************************************************************************************************
//**************************************************************************************************************************************
//**************************************************************************************************************************************


MidiToComConverter::MidiToComConverter(QObject *parent) :
    QObject(parent),transposeValue(0)
{
}

void MidiToComConverter::setDriveMapping(quint16 track, QString newMapString){
    //change drive map for one track only
    QStringList driveList = newMapString.split(",", QString::SkipEmptyParts);
    trackDrivesMap.remove(track);
    while(!driveList.isEmpty()){
        QString currentString=driveList.takeLast();
        if(!currentString.isEmpty()){
            int driveNum = currentString.toInt();
            if(!trackDrivesMap.contains((quint16)track,driveNum)){
                trackDrivesMap.insert((quint16)track,driveNum);
            }
        }
    }

    emit driveMapUpdated(getDriveStringMap());
}


void MidiToComConverter::loadDriveMapFromSettings(QStringList mapStringList){
    //convert stringlist from settings file to drive map
    trackDrivesMap.clear();
    for(int i = 0;i<mapStringList.size();i++){
        QStringList driveList = mapStringList.at(i).split(",", QString::SkipEmptyParts);
        while(!driveList.isEmpty()){
            QString currentString=driveList.takeLast();
            if(!currentString.isEmpty()){
                int driveNum = currentString.toInt();
                if(!trackDrivesMap.contains((quint16)i,driveNum)){
                    trackDrivesMap.insert((quint16)i,driveNum);
                }
            }
        }
    }
    emit driveMapUpdated(getDriveStringMap());
}

QMap<int,QString> MidiToComConverter::getDriveStringMap(void){
    //convert drive map into string for displaying in trackTable
    QMap<int,QString> mapToReturn;
    foreach(quint16 trackNum, trackDrivesMap.keys()){
        QString stringToAppend;
        QList<int> driveList=trackDrivesMap.values(trackNum);
        while(!driveList.isEmpty()){
            stringToAppend.append(QString::number(driveList.takeFirst()));
            if(!driveList.isEmpty()){
                stringToAppend.append(",");
            }
        }
        mapToReturn.insert(trackNum,stringToAppend);
    }
    return mapToReturn;
}

void MidiToComConverter::eventListToConvert(QList<MidiEvent> eventList){
    while(!eventList.isEmpty()){
        processEvent(eventList.takeFirst());
    }
}

void MidiToComConverter::processEvent(MidiEvent eventToProcess){
    quint32 eventParam=eventToProcess.getParam();
    switch(eventToProcess.getType()){
    //LOOKING FOR A UNUSED DRIVE TO PLAY
    case MidiEvent::noteOn:{
        quint16 trackNum = eventToProcess.getTrackNum();
        if(trackDrivesMap.contains(trackNum)){
            foreach(int driveNum,trackDrivesMap.values(trackNum)){
               if(driveNum>=dummyDrivesList.size()){continue;}
                DummyFloppyDrive* currentDrive=dummyDrivesList.at(driveNum);
                if(!currentDrive->getIsPlaying()){
                    uint note = (uint)(eventParam&0x0000007F);
                    currentDrive->play(note,transposeValue);
                    playingDrivesMap.insert(eventParam,currentDrive);
                    break;
                }
            }
        }
        break;
    }
    //DEACTIVATING DRIVE
    case MidiEvent::noteOff:
        if(playingDrivesMap.contains(eventParam)){
            DummyFloppyDrive* currentDrive=playingDrivesMap.take(eventParam);
            currentDrive->stop();
        }
        break;
    default:
        break;
    }
}


void MidiToComConverter::setAvailableDrivesNum(uint availablesDrives){
    //make dummydrivelist size fit availables drive value
    while((uint)dummyDrivesList.size()<availablesDrives){
        DummyFloppyDrive* createdDrive = new DummyFloppyDrive(dummyDrivesList.size());
        connect(createdDrive,&DummyFloppyDrive::comData,this,&MidiToComConverter::comData);
        dummyDrivesList.append(createdDrive);
    }
    while((uint)dummyDrivesList.size()>availablesDrives){
        DummyFloppyDrive* currentDrive=dummyDrivesList.takeLast();
        if(currentDrive->getIsPlaying()){
            currentDrive->stop();
            playingDrivesMap.remove(playingDrivesMap.key(currentDrive));
        }
        currentDrive->deleteLater();
    }
}

void MidiToComConverter::transposeCommand(int newTransposeValue){
    //for each drive transpose and send new drive On command
    transposeValue=newTransposeValue;
    foreach(DummyFloppyDrive* currentDrive,dummyDrivesList){
        if(currentDrive->getIsPlaying()){
            currentDrive->updateTransposedNote(transposeValue);
        }
    }
}

int MidiToComConverter::getTransposeValue(void){
    return transposeValue;
}

void MidiToComConverter::stopTrack(quint16 trackNum){
    //stop all drives playing track number trackNum
    if(trackDrivesMap.contains(trackNum)){
        foreach(int driveNum,trackDrivesMap.values(trackNum)){
            DummyFloppyDrive* currentDrive=dummyDrivesList.at(driveNum);
            if(currentDrive->getIsPlaying()){
                currentDrive->stop();
                playingDrivesMap.remove(playingDrivesMap.key(currentDrive));
            }
        }
    }
}

void MidiToComConverter::onPlayerStateChanged(MidiFile::PlayerState state){
    switch(state){
    case MidiFile::PlayerStop:
        foreach(DummyFloppyDrive* currentDrive,dummyDrivesList){
            currentDrive->stop();
#ifdef GENERATE_NOTE_ARRAY
            currentDrive->printNoteReport();
#endif
        }
        playingDrivesMap.clear();
        break;

    case MidiFile::PlayerPause:
        foreach(DummyFloppyDrive* currentDrive,playingDrivesMap.values()){
            currentDrive->pause();
        }
        break;

    case MidiFile::PlayerPlay:
        foreach(DummyFloppyDrive* currentDrive,playingDrivesMap.values()){
            currentDrive->resume();
        }
        break;

    default:
        break;
    }
}

