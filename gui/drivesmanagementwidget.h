#ifndef DRIVESMANAGEMENTWIDGET_H
#define DRIVESMANAGEMENTWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QFormLayout>
#include <QSpinBox>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>

class DrivesManagementWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DrivesManagementWidget(QWidget *parent = 0);
    ~DrivesManagementWidget();

    int getAvailableDrivesNumber(void);

    
signals:
    void availableDrivesNumberChanged(uint availableDrives);
    void autoUpdateNeededDrivesWhilePlaying(bool autoUpdate);
    
public slots:
    void neededDrivesNumberChanged(uint neededDrives);
    void setAvailableDrivesNumber(int availableDrives);
    void setAutoUpdate(bool autoUpdate);

private slots:
    void driveSpinBoxValueChanged(int newValue);

private:
    void updateDrivesNeededLabelText(void);

    int drivesNeeded;

    QLabel* neededDrivesLabel;
    QSpinBox* availableDrivesSpinBox;
    QCheckBox* autoUpdateCheckBox;
    QHBoxLayout* mainLayout;
    QGroupBox* mainGroupBox;
    QFormLayout* formLayout;
};

#endif // DRIVESMANAGEMENTWIDGET_H
