#include "transposecontrolwidget.h"

#define MINIMUM -10
#define MAXIMUM 10
#define DEFAULT 0

#define TIMER_INTERVAL 500

TransposeControlWidget::TransposeControlWidget(QWidget *parent) :
    QWidget(parent),maxTransposeValue(0),minTransposeValue(0),currentTransposeValue(0),
    originalHighestNote(127),originalLowestNote(0),transposedHighestNote(0),transposedLowestNote(0)
{
    mainGroupBox = new QGroupBox("Transposer",this);
    mainLayout = new QHBoxLayout(this);
    mainLayout->addWidget(mainGroupBox);
    mainLayout->setMargin(0);
    this->setLayout(mainLayout);


    //SETTING UP SPINBOX & BUTTON LAYOUT
    spinBox = new QSpinBox(mainGroupBox);
    spinBox->setFixedSize(100,20);
    spinBox->setSuffix(" octave");

    resetButton = new QPushButton("Reset",mainGroupBox);
    resetButton->setFixedHeight(20);

    auxHorizontalLayout = new QHBoxLayout();
    auxHorizontalLayout->setMargin(0);
    auxHorizontalLayout->addWidget(spinBox);
    auxHorizontalLayout->addWidget(resetButton);


    //SETTING UP FORM LAYOUT
    originalHighestNoteLabel = new QLabel(mainGroupBox);
    originalLowestNoteLabel = new QLabel(mainGroupBox);

    transposedHighestNoteLabel = new QLabel(mainGroupBox);
    transposedLowestNoteLabel = new QLabel(mainGroupBox);

    formLayout = new QFormLayout();
    formLayout->addRow("Original highest note :",originalHighestNoteLabel);
    formLayout->addRow("Original lowest note :",originalLowestNoteLabel);
    formLayout->addRow(auxHorizontalLayout);
    formLayout->addRow("Transposed highest note :",transposedHighestNoteLabel);
    formLayout->addRow("Transposed lowest note :",transposedLowestNoteLabel);

    slider = new QSlider(Qt::Vertical,mainGroupBox);
    slider->setFixedWidth(20);

    horizontalLine = new QFrame();
    horizontalLine->setFrameShape(QFrame::HLine);
    horizontalLine->setFrameShadow(QFrame::Sunken);

    autoButton = new QPushButton("Auto transpose");

    autoCheckBox = new QCheckBox("Auto transpose on highest/lowest note change",mainGroupBox);

    frequencySpinBox = new QSpinBox();
    frequencySpinBox->setSuffix(" Hz");
    frequencySpinBox->setRange(9,15000);
    frequencySpinBox->setSpecialValueText("Off");
    connect(frequencySpinBox,SIGNAL(valueChanged(int)),this,SLOT(frequencySpinBoxValueChanged()));

    frequencyLayout = new QFormLayout();
    frequencyLayout->addRow("Floppy drives cut-off freq :",frequencySpinBox);

    mainGridLayout = new QGridLayout();
    mainGridLayout->addWidget(slider,0,0);
    mainGridLayout->addLayout(formLayout,0,1);
    mainGridLayout->addWidget(horizontalLine,1,0,1,2);
    mainGridLayout->addWidget(autoButton,2,0,1,2);
    mainGridLayout->addWidget(autoCheckBox,3,0,1,2);
    mainGridLayout->addLayout(frequencyLayout,4,0,1,2);
    mainGroupBox->setLayout(mainGridLayout);

    transposedHighestNoteTimer = new QTimer(this);
    transposedHighestNoteTimer->setInterval(TIMER_INTERVAL);
    transposedHighestNoteTimer->setSingleShot(true);
    connect(transposedHighestNoteTimer,SIGNAL(timeout()),this,SLOT(transposedHighestNoteTimerTick()));

    transposedLowestNoteTimer = new QTimer(this);
    transposedLowestNoteTimer->setInterval(TIMER_INTERVAL);
    transposedLowestNoteTimer->setSingleShot(true);
    connect(transposedLowestNoteTimer,SIGNAL(timeout()),this,SLOT(transposedLowestNoteTimerTick()));

    spinBox->setMinimum(MINIMUM);
    slider->setMinimum(MINIMUM);

    spinBox->setMaximum(MAXIMUM);
    slider->setMaximum(MAXIMUM);

    spinBox->setValue(DEFAULT);
    slider->setValue(DEFAULT);

    setUiEnabled(false);

    connect(slider,SIGNAL(actionTriggered(int)),this,SLOT(actionOnSlider()));
    connect(spinBox,SIGNAL(valueChanged(int)),this,SLOT(valueUpdatedByUser(int)));
    connect(resetButton,SIGNAL(clicked()),this,SLOT(reset()));
    connect(autoButton,SIGNAL(clicked()),this,SLOT(autoButtonClicked()));
}

void TransposeControlWidget::actionOnSlider(){
    valueUpdatedByUser(slider->sliderPosition());
}

void TransposeControlWidget::reset(void){
    currentTransposeValue=0;
    updateUI();
    emit transposeCommand(0);
}


void TransposeControlWidget::valueUpdatedByUser(int value){
    if(value>maxTransposeValue){
        value=maxTransposeValue;
        blinkTransposedHighestNoteLabel();
    }
    if(value<minTransposeValue){
        value=minTransposeValue;
        blinkTransposedLowestNoteLabel();
    }
    currentTransposeValue=value;
    calculateTransposedLowestAndHighestNote();
    updateUI();
    emit transposeCommand(value);
}

void TransposeControlWidget::originalHighestAndLowestNote(quint8 newOriginalLowestNote, quint8 newOriginalHighestNote){
    setUiEnabled(false);
    frequencySpinBox->setStyleSheet("");
    originalHighestNote=newOriginalHighestNote;
    originalLowestNote=newOriginalLowestNote;
    calculateTransposeRange();
    currentTransposeValue=qMax(currentTransposeValue,minTransposeValue);
    currentTransposeValue=qMin(currentTransposeValue,maxTransposeValue);
    if(autoCheckBox->isChecked()){
        calculateAutoTranspose();
    }
    calculateTransposedLowestAndHighestNote();
    updateUI();
    emit transposeCommand(currentTransposeValue);
    setUiEnabled(true);
}

void TransposeControlWidget::calculateTransposeRange(){
    minTransposeValue=-(originalLowestNote/12);
    maxTransposeValue=(127-originalHighestNote)/12;
}

void TransposeControlWidget::calculateAutoTranspose(void){
    if(frequencySpinBox->value()<=frequencySpinBox->minimum()){return;}
    int cutOffFrequency=frequencySpinBox->value();
    int maxNote = MidiFile::frequencyToMidiNote(cutOffFrequency)-1;

    currentTransposeValue=0;
    calculateTransposedLowestAndHighestNote();
    while(currentTransposeValue>minTransposeValue&&transposedHighestNote>maxNote){
        currentTransposeValue--;
        calculateTransposedLowestAndHighestNote();
    }

}

void TransposeControlWidget::autoButtonClicked(void){
    calculateAutoTranspose();
    updateUI();
}

void TransposeControlWidget::calculateTransposedLowestAndHighestNote(){
    transposedLowestNote=originalLowestNote+currentTransposeValue*12;
    transposedHighestNote=originalHighestNote+currentTransposeValue*12;
}

void TransposeControlWidget::updateUI(void){
    originalHighestNoteLabel->setText(QString::number(originalHighestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency(originalHighestNote))+" Hz)");
    originalLowestNoteLabel->setText(QString::number(originalLowestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency(originalLowestNote))+" Hz)");
    spinBox->setValue(currentTransposeValue);
    slider->setValue(currentTransposeValue);
    transposedHighestNoteLabel->setText(QString::number(transposedHighestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency(transposedHighestNote))+" Hz)");
    transposedLowestNoteLabel->setText(QString::number(transposedLowestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency(transposedLowestNote))+" Hz)");
    updateFrequencySpinBoxColor();
}

void TransposeControlWidget::updateFrequencySpinBoxColor(void){
    if(frequencySpinBox->value()==frequencySpinBox->minimum()){
        frequencySpinBox->setStyleSheet("");
    }
    else if(frequencySpinBox->value()>=MidiFile::midiNoteToFrequency(transposedHighestNote)){
        frequencySpinBox->setStyleSheet("QSpinBox { color : green }");
    }
    else{
        frequencySpinBox->setStyleSheet("QSpinBox { color : red }");
    }
}

void TransposeControlWidget::setUiEnabled(bool enabled){
    slider->setEnabled(enabled);
    spinBox->setEnabled(enabled);
    resetButton->setEnabled(enabled);
    autoButton->setEnabled(enabled);
}

void TransposeControlWidget::blinkTransposedHighestNoteLabel(void){
    transposedHighestNoteLabel->setStyleSheet("QLabel { color: red }");
    transposedHighestNoteTimer->start();
}

void TransposeControlWidget::blinkTransposedLowestNoteLabel(void){
    transposedLowestNoteLabel->setStyleSheet("QLabel { color: red }");
    transposedLowestNoteTimer->start();
}

void TransposeControlWidget::transposedHighestNoteTimerTick(void){
    transposedHighestNoteLabel->setStyleSheet("");
}

void TransposeControlWidget::transposedLowestNoteTimerTick(void){
    transposedLowestNoteLabel->setStyleSheet("");
}

void TransposeControlWidget::frequencySpinBoxValueChanged(void){
    autoCheckBox->setChecked(true);
}

void TransposeControlWidget::transposeSetting(int value){
    currentTransposeValue=value;
    currentTransposeValue=qMax(currentTransposeValue,minTransposeValue);
    currentTransposeValue=qMin(currentTransposeValue,maxTransposeValue);
    updateUI();
    emit transposeCommand(currentTransposeValue);
}

int TransposeControlWidget::getCutOffFrequency(void){
    return frequencySpinBox->value();
}

void TransposeControlWidget::setCutOffFrequency(int newFrequency){
    newFrequency=qMin(newFrequency,frequencySpinBox->maximum());
    newFrequency=qMax(newFrequency,frequencySpinBox->minimum());
    frequencySpinBox->setValue(newFrequency);
}

bool TransposeControlWidget::getAutoChecked(void){
    return autoCheckBox->isChecked();
}

void TransposeControlWidget::setAutoChecked(bool checked){
    autoCheckBox->setChecked(checked);
}

TransposeControlWidget::~TransposeControlWidget(){
    delete spinBox;
    delete resetButton;
    delete auxHorizontalLayout;

    delete originalHighestNoteLabel;
    delete originalLowestNoteLabel;
    delete transposedHighestNoteLabel;
    delete transposedLowestNoteLabel;
    delete transposedHighestNoteTimer;
    delete transposedLowestNoteTimer;
    delete formLayout;

    delete slider;
    delete horizontalLine;
    delete frequencySpinBox;
    delete frequencyLayout;
    delete autoButton;
    delete autoCheckBox;
    delete mainGridLayout;

    delete mainGroupBox;
    delete mainLayout;
}
