#include "drivesmanagementwidget.h"

DrivesManagementWidget::DrivesManagementWidget(QWidget *parent) :
    QWidget(parent),drivesNeeded(0)
{
    mainGroupBox = new QGroupBox("Drives management",this);

    neededDrivesLabel = new QLabel();

    availableDrivesSpinBox = new QSpinBox();
    availableDrivesSpinBox->setSuffix(" drives");
    availableDrivesSpinBox->setSpecialValueText("No drive");
    availableDrivesSpinBox->setRange(0,64);
    connect(availableDrivesSpinBox,SIGNAL(valueChanged(int)),this,SLOT(driveSpinBoxValueChanged(int)));

    autoUpdateCheckBox = new QCheckBox("Auto-update while playing");
    autoUpdateCheckBox->setChecked(true);
    connect(autoUpdateCheckBox,SIGNAL(clicked(bool)),this,SIGNAL(autoUpdateNeededDrivesWhilePlaying(bool)));

    formLayout = new QFormLayout();
    formLayout->setContentsMargins(8,4,8,0);
    formLayout->setVerticalSpacing(3);
    formLayout->addRow("Required drives : ",neededDrivesLabel);
    formLayout->addRow("Available drives : ",availableDrivesSpinBox);
    formLayout->addRow(autoUpdateCheckBox);
    mainGroupBox->setLayout(formLayout);

    mainLayout = new QHBoxLayout();
    mainLayout->setMargin(0);
    mainLayout->addWidget(mainGroupBox);
    this->setLayout(mainLayout);

    updateDrivesNeededLabelText();
}

void DrivesManagementWidget::driveSpinBoxValueChanged(int newValue){
    updateDrivesNeededLabelText();
    emit availableDrivesNumberChanged(newValue);
}

void DrivesManagementWidget::neededDrivesNumberChanged(uint neededDrives){
    drivesNeeded=neededDrives;
    updateDrivesNeededLabelText();
}

void DrivesManagementWidget::updateDrivesNeededLabelText(void){
    if(drivesNeeded==0){
        if(availableDrivesSpinBox->value()==availableDrivesSpinBox->minimum()){
            neededDrivesLabel->setText("<font color = \"blue\">Set available drives number</font>");
        }
        else {
            neededDrivesLabel->setText("No drive required");
        }
    }
    else if(availableDrivesSpinBox->value()<drivesNeeded){
        neededDrivesLabel->setText(" <font color = \"red\">"+QString::number(drivesNeeded)+" (not enough drives available)</font>");
    }
    else{
        neededDrivesLabel->setText(" <font color = \"green\">"+QString::number(drivesNeeded)+" (enough drives available)</font>");
    }
}

void DrivesManagementWidget::setAvailableDrivesNumber(int availableDrives){
    availableDrives=qMin(availableDrives,availableDrivesSpinBox->maximum());
    availableDrives=qMax(availableDrives,availableDrivesSpinBox->minimum());
    availableDrivesSpinBox->setValue(availableDrives);
}

int DrivesManagementWidget::getAvailableDrivesNumber(void){
    return availableDrivesSpinBox->value();
}

void DrivesManagementWidget::setAutoUpdate(bool autoUpdate){
    autoUpdateCheckBox->setChecked(autoUpdate);
    emit autoUpdateNeededDrivesWhilePlaying(autoUpdate);
}

DrivesManagementWidget::~DrivesManagementWidget(){
    delete neededDrivesLabel;
    delete availableDrivesSpinBox;
    delete autoUpdateCheckBox;

    delete formLayout;
    delete mainGroupBox;
    delete mainLayout;
}
