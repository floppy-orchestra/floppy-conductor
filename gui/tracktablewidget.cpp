#include "tracktablewidget.h"

TrackTableWidget::TrackTableWidget(QWidget *parent) :
    QTableWidget(parent)
{
    setColumnCount(7);
    //SETTING HORIZONTAL HEADERS LABEL
    QStringList horizontalHeaderLabels;
    horizontalHeaderLabels<< "" <<"Title"<<"Total notes"<<"Drive req."<<"Highest note"<<"Lowest note"<<"Drives";
    setHorizontalHeaderLabels(horizontalHeaderLabels);
    resetSectionsSizes();
    connect(this,SIGNAL(cellClicked(int,int)),this,SLOT(cellHasBeenClicked(int,int)));
    connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(cellHasBeenChanged(int,int)));
}

void TrackTableWidget::updateTrackTable(QList<MidiFile::TrackSummary> newTrackList){
    trackList = newTrackList;
    updateDisplay();
}

void TrackTableWidget::updateDriveMap(QMap<int,QString> newDriveStringMap){
    driveStringMap=newDriveStringMap;
    updateDisplay();
}

void TrackTableWidget::updateDisplay(void){
    isEditing=true;
    clearContents();
    quint16 numTracks = trackList.size();
    setRowCount(numTracks);
    for(quint16 i = 0;i<numTracks;i++){
        MidiFile::TrackSummary currentTrack = trackList.at(i);

        QIcon statusIcon;
        statusIcon.addFile(currentTrack.trackIsPlayed ? ":iconStatusPlayed" : ":iconStatusNotPlayed",QSize(24,24));

        QTableWidgetItem *trackIconStatus = new QTableWidgetItem();
        trackIconStatus->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        trackIconStatus->setIcon(statusIcon);
        setItem(i ,0, trackIconStatus);

        QTableWidgetItem *trackTitle = new QTableWidgetItem();
        trackTitle->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        trackTitle->setText(currentTrack.trackName);
        setItem(i, 1, trackTitle);

        QTableWidgetItem *trackNoteCount = new QTableWidgetItem();
        trackNoteCount->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        trackNoteCount->setText(QString::number(currentTrack.trackNoteCount)+" note"+(currentTrack.trackNoteCount>1?"s":""));
        setItem(i, 2, trackNoteCount);

        QTableWidgetItem *trackMaxSimultaneousNotes = new QTableWidgetItem();
        trackMaxSimultaneousNotes->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        trackMaxSimultaneousNotes->setText(QString::number(currentTrack.trackMaxSimultaneousNotes)+" drive"+(currentTrack.trackMaxSimultaneousNotes>1?"s":""));
        setItem(i, 3, trackMaxSimultaneousNotes);

        QTableWidgetItem *trackHighestNote = new QTableWidgetItem();
        trackHighestNote->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        if(currentTrack.trackIsMusical){
            trackHighestNote->setText(QString::number(currentTrack.trackHighestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency((int)currentTrack.trackHighestNote))+" Hz)");
        }
        setItem(i, 4, trackHighestNote);

        QTableWidgetItem *trackLowestNote = new QTableWidgetItem();
        trackLowestNote->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled : Qt::NoItemFlags );
        if(currentTrack.trackIsMusical){
            trackLowestNote->setText(QString::number(currentTrack.trackLowestNote)+" ("+QString::number(MidiFile::midiNoteToFrequency((int)currentTrack.trackLowestNote))+" Hz)");
        }
        setItem(i, 5, trackLowestNote);

        QTableWidgetItem *trackDriveMap = new QTableWidgetItem(driveStringMap.contains(i) ? driveStringMap.value(i):"");
        trackDriveMap->setFlags(currentTrack.trackIsMusical ? Qt::ItemIsEnabled|Qt::ItemIsEditable : Qt::NoItemFlags );
        setItem(i,6,trackDriveMap);

    }
    isEditing=false;
}

void TrackTableWidget::autoAdaptAllSectionsSize(void){
    resetSectionsSizes();
    autoAdaptTitleSectionSize();
}


void TrackTableWidget::autoAdaptTitleSectionSize(void){
    int titleSectionNextSize = maximumViewportSize().width();
    for(int i = 0;i<horizontalHeader()->count();i++){
        if(i==1){continue;}
        titleSectionNextSize-=horizontalHeader()->sectionSize(i);
    }
    if(verticalScrollBar()){
        if(verticalScrollBar()->isVisible()){
            titleSectionNextSize-=verticalScrollBar()->width();
        }
    }
    titleSectionNextSize=qMax(titleSectionNextSize,100);
    horizontalHeader()->resizeSection(1,titleSectionNextSize);
}

void TrackTableWidget::resetSectionsSizes(void){
    verticalHeader()->setDefaultSectionSize(24);
    horizontalHeader()->resizeSection(0,22);
    horizontalHeader()->resizeSection(1,159);
    horizontalHeader()->resizeSection(2,70);
    horizontalHeader()->resizeSection(3,60);
    horizontalHeader()->resizeSection(4,100);
    horizontalHeader()->resizeSection(5,100);
    horizontalHeader()->resizeSection(6,65);
}

void TrackTableWidget::cellHasBeenClicked(int row, int column){
    if(column==0){
        emit toggleTrackPlayed(row);
    }
}

void TrackTableWidget::cellHasBeenChanged(int row, int column){
    if(!isEditing&&column==6){
        emit driveMappingEditedByUser((quint16)row,item(row,column)->text());
    }
}
