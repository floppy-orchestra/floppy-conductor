#ifndef TRACKTABLEWIDGET_H
#define TRACKTABLEWIDGET_H

#include <QTableWidget>
#include <QStringList>
#include <QObject>
#include <QHeaderView>
#include <QIcon>
#include <QScrollBar>
#include <QResizeEvent>
#include <QSize>
#include "midifile.h"

class TrackTableWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit TrackTableWidget(QWidget *parent = 0);
    
signals:
    void toggleTrackPlayed(quint16 track);
    void driveMappingEditedByUser(quint16 track, QString newDriveMapString);
public slots:
    void updateTrackTable(QList<MidiFile::TrackSummary> newTrackList);
    void updateDriveMap(QMap<int, QString> newDriveStringMap);
    void autoAdaptAllSectionsSize(void);

private slots:
    void cellHasBeenClicked(int row, int column);
    void cellHasBeenChanged(int row, int column);
private:
    void updateDisplay(void);
    void resetSectionsSizes(void);
    void autoAdaptTitleSectionSize(void);

    bool isEditing;
    QList<MidiFile::TrackSummary> trackList;
    QMap<int,QString> driveStringMap;
};

#endif // TRACKTABLEWIDGET_H
