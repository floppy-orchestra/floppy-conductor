#ifndef LINKSTATUSWIDGET_H
#define LINKSTATUSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QIcon>
#include <QLabel>
#include <QComboBox>
#include "../comlink.h"

class linkStatusWidget : public QWidget
{
    Q_OBJECT
public:
    explicit linkStatusWidget(QWidget *parent = 0);

signals:
    void toggleLinkState(void);
    void comPortButtonClicked();

public slots:
    void updatePortStatus(bool isConnected);
    void updateSelectedPort(ComLink::SerialSettings settings);
    void updateStatusString(QString status);


private:
    QMap <QSerialPort::Parity,QString> parityTextMap;
    QMap <QSerialPort::FlowControl,QString> flowcontrolTextMap;

    QIcon iconConnected;
    QIcon iconUnconnected;

    QPushButton* selectedPortButton;
    QLabel* statusLabel;

    QPushButton* toggleStateButton;
    QGridLayout* secondLayout;

    QGroupBox* mainGroupBox;
    QHBoxLayout* mainLayout;
};

#endif // LINKSTATUSWIDGET_H
