#ifndef MODEDISPLAYWIDGET_H
#define MODEDISPLAYWIDGET_H

#include <QWidget>
#include "midifile.h"
#include <QIcon>
class ModeDisplayWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ModeDisplayWidget(QWidget *parent = 0);
    void paintEvent(QPaintEvent *);
    
signals:
    
public slots:
    void setMode(MidiFile::PlayerState newState);

private :
    QIcon iconPlay;
    QIcon iconPause;
    QIcon iconStop;
    QIcon iconLocked;
    QIcon iconClosed;
    QIcon icon;
    
};

#endif // MODEDISPLAYWIDGET_H
