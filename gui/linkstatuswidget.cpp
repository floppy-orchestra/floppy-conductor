#include "linkstatuswidget.h"

linkStatusWidget::linkStatusWidget(QWidget *parent) :
    QWidget(parent)
{
    parityTextMap.insert(QSerialPort::NoParity,"N");
    parityTextMap.insert(QSerialPort::EvenParity,"E");
    parityTextMap.insert(QSerialPort::OddParity,"O");
    parityTextMap.insert(QSerialPort::SpaceParity,"S");
    parityTextMap.insert(QSerialPort::MarkParity,"M");

    //flowcontrolTextMap.insert(QSerialPort::NoFlowControl,"nofc");
    flowcontrolTextMap.insert(QSerialPort::HardwareControl,"hwfc");
    flowcontrolTextMap.insert(QSerialPort::SoftwareControl,"swfc");

    iconConnected.addFile(":iconLinkConnected",QSize(256,256));
    iconUnconnected.addFile(":iconLinkUnconnected",QSize(256,256));

    mainGroupBox = new QGroupBox("Link control",this);

    selectedPortButton = new QPushButton(this);
    selectedPortButton->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Fixed);
    connect(selectedPortButton,SIGNAL(clicked(bool)),this,SIGNAL(comPortButtonClicked()));

    statusLabel = new QLabel(this);
    updateStatusString("Select a COM port");

    toggleStateButton = new QPushButton(this);
    toggleStateButton->setIconSize(QSize(36,36));
    toggleStateButton->setFixedSize(48,48);
    connect(toggleStateButton,SIGNAL(clicked()),this,SIGNAL(toggleLinkState()));

    updateSelectedPort(ComLink::SerialSettings());

    secondLayout = new QGridLayout(mainGroupBox);
    secondLayout->setContentsMargins(6,2,6,6);
    secondLayout->setVerticalSpacing(0);
    secondLayout->addWidget(toggleStateButton,0,0,2,1);
    secondLayout->addWidget(selectedPortButton,0,1);
    secondLayout->addWidget(statusLabel,1,1);

    mainLayout = new QHBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->addWidget(mainGroupBox);
    this->setLayout(mainLayout);

    updatePortStatus(false);
}

void linkStatusWidget::updatePortStatus(bool isConnected){
    toggleStateButton->setIcon(isConnected ? iconConnected : iconUnconnected);
    selectedPortButton->setDisabled(isConnected);
}

void linkStatusWidget::updateStatusString(QString status){
    QString labelText("Status : %1");
    statusLabel->setText(labelText.arg(status));
}

void linkStatusWidget::updateSelectedPort(ComLink::SerialSettings settings){
    QString text;
    if(settings.portName.simplified().isEmpty()){
        text = QString("Select device");
    }
    else{

        text = QString("%1 %2 %3%4%5 %6").arg(settings.portName,
                                               QString::number(settings.baudRate),
                                               QString::number(settings.dataBits),
                                               parityTextMap.value(settings.parity),
                                               QString::number((settings.stopBits == QSerialPort::OneAndHalfStop ) ? 1.5 :(int)settings.stopBits),
                                               flowcontrolTextMap.value(settings.flowControl));
    }
    selectedPortButton->setText(text);
}
