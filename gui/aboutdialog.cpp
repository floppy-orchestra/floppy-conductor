#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":iconMainWindow"));
    this->setWindowTitle("About Floppy Conductor");
    ui->versionLabel->setText("Floppy Conductor\nBy Pila\n\nVersion "+QString::fromLocal8Bit(VERSION)+"\n\nCompiled on :\n"+QString::fromLocal8Bit(BUILDDATE)/*+"\n"+QString::fromLocal8Bit(BUILDTIME)*/);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
