#ifndef PLAYCONTROLWIDGET_H
#define PLAYCONTROLWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QIcon>
#include <QLabel>
#include <QTimer>
#include "midifile.h"

class MidiFile;

class playControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit playControlWidget(QWidget *parent = 0);
    ~playControlWidget();

    QString getLastOpenedFilePath(void);
    void setLastOpenedFilePath(QString lastOpenedFilePath);

signals:
    void control(MidiFile::PlayerState newStatus);
    void open(QString filePath);
    
public slots:
    void newStatus(MidiFile::PlayerState newStatus);
    void updateFileInfo(QString newFileName, quint16 numTracks, quint16 numMusicalTracks, QString newTrackDuration);

private slots:
    void buttonOpenClicked(void);
    void buttonPlayClicked(void);
    void buttonPauseClicked(void);
    void buttonStopClicked(void);
    void scrollingTick(void);

private:
    void updateStatus(void);
    void checkLabelWidth(void);
    void resizeEvent(QResizeEvent *event);

    QHBoxLayout* layoutMain;

    QString filePath;

    //CONTROL PART
    MidiFile::PlayerState status;
    QIcon playIcon;
    QIcon pauseIcon;
    QIcon stopIcon;
    QIcon openIcon;
    QPushButton* playButton;
    QPushButton* pauseButton;
    QPushButton* stopButton;
    QPushButton* openButton;
    QGroupBox* groupBoxControl;
    QHBoxLayout* layoutControl;

    //INFOS PART
    QGroupBox* groupBoxInfo;
    QString fileName;
    QLabel* fileNameLabel;
    QLabel* trackDurationLabel;
    QLabel* numTrackLabel;
    QTimer* scrollingTimer;
    unsigned int scrollCounter;
    QFormLayout* layoutInfo;
};

#endif // PLAYCONTROLWIDGET_H
