#include "modedisplaywidget.h"
#include <QPixmap>
#include <QPainter>

ModeDisplayWidget::ModeDisplayWidget(QWidget *parent) :
    QWidget(parent)
{
    iconPlay.addFile(":iconModePlay");
    iconPause.addFile(":iconModePause");
    iconStop.addFile(":iconModeStop");
    iconLocked.addFile(":iconModeLocked");
    iconClosed.addFile(":iconModeClosed");
    icon=iconClosed;
    setFixedSize(20,20);
    update();
}

void ModeDisplayWidget::setMode(MidiFile::PlayerState newState){
    switch(newState){
        case MidiFile::PlayerPlay:
            icon=iconPlay;
        break;
        case MidiFile::PlayerPause:
            icon=iconPause;
        break;
        case MidiFile::PlayerStop:
            icon=iconStop;
        break;
        case MidiFile::PlayerLocked:
            icon=iconLocked;
        break;
        case MidiFile::PlayerClosed:
            icon=iconClosed;
        break;
    }
    update();
}

void ModeDisplayWidget::paintEvent(QPaintEvent *){
    QPainter painter(this);
    QPixmap pixmap;
    pixmap=icon.pixmap(QSize(20, 20));
    painter.drawPixmap(QPoint(0,0), pixmap);
}
