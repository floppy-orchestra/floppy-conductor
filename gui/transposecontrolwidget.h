#ifndef TRANSPOSECONTROLEWIDGET_H
#define TRANSPOSECONTROLEWIDGET_H

#include <QWidget>
#include <QSlider>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QFormLayout>
#include <QFrame>
#include <QTimer>
#include <QMessageBox>
#include "midifile.h"

class TransposeControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TransposeControlWidget(QWidget *parent = 0);
    ~TransposeControlWidget();
    int getCutOffFrequency(void);
    void setCutOffFrequency(int newFrequency);
    bool getAutoChecked(void);
    void setAutoChecked(bool checked);
    
signals:
    void transposeCommand(int value);

public slots:
    void originalHighestAndLowestNote(quint8 newOriginalLowestNote, quint8 newOriginalHighestNote);
    void reset(void);
    void transposeSetting(int value);

private slots:
    void valueUpdatedByUser(int value);
    void actionOnSlider();
    void autoButtonClicked(void);
    void frequencySpinBoxValueChanged(void);
    void updateFrequencySpinBoxColor(void);
    void transposedHighestNoteTimerTick(void);
    void transposedLowestNoteTimerTick(void);

private:
    void setUiEnabled(bool enabled);
    void calculateTransposeRange(void);
    void calculateTransposedLowestAndHighestNote(void);
    void calculateAutoTranspose(void);
    void updateUI(void);
    void blinkTransposedHighestNoteLabel(void);
    void blinkTransposedLowestNoteLabel(void);

    int maxTransposeValue;
    int minTransposeValue;
    int currentTransposeValue;
    quint8 originalHighestNote;
    quint8 originalLowestNote;
    quint8 transposedHighestNote;
    quint8 transposedLowestNote;

    //FOR GUI
    QSpinBox* spinBox;
    QPushButton* resetButton;
    QHBoxLayout* auxHorizontalLayout;

    QLabel* originalHighestNoteLabel;
    QLabel* originalLowestNoteLabel;
    QLabel* transposedHighestNoteLabel;
    QLabel* transposedLowestNoteLabel;
    QTimer* transposedHighestNoteTimer;
    QTimer* transposedLowestNoteTimer;
    QFormLayout* formLayout;

    QSlider* slider;
    QFrame* horizontalLine;
    QCheckBox* autoCheckBox;
    QSpinBox* frequencySpinBox;
    QPushButton* autoButton;
    QFormLayout* frequencyLayout;
    QGridLayout* mainGridLayout;

    QGroupBox* mainGroupBox;
    QHBoxLayout* mainLayout;
};

#endif // TRANSPOSECONTROLEWIDGET_H
