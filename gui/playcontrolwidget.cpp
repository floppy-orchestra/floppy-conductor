#include "playcontrolwidget.h"
#include <QFileDialog>
#include <QSizePolicy>

#define SCROLLING_INTERVAL 500
#define SCROLLING_NB_CHAR 1
#define DELAY_BEFORE_SCROLLING 10 //X*timer interval

playControlWidget::playControlWidget(QWidget *parent) :
    QWidget(parent)
{
    //SETTING CONTROL GROUPBOX
    groupBoxControl=new QGroupBox("Player control",this);

    openIcon.addFile(":iconButtonOpen",QSize(32,32),QIcon::Normal);
    openIcon.addFile(":iconButtonOpenLocked",QSize(32,32),QIcon::Disabled);

    openButton = new QPushButton(groupBoxControl);
    openButton->setFlat(true);
    openButton->setIcon(openIcon);
    openButton->setIconSize(QSize(32,32));
    openButton->setFixedSize(40,36);

    playIcon.addFile(":iconButtonPlay",QSize(32,32),QIcon::Normal);
    playIcon.addFile(":iconButtonPlayLocked",QSize(32,32),QIcon::Disabled);

    playButton = new QPushButton(groupBoxControl);
    playButton->setFlat(true);
    playButton->setIcon(playIcon);
    playButton->setIconSize(QSize(32,32));
    playButton->setFixedSize(40,36);

    pauseIcon.addFile(":iconButtonPause",QSize(32,32),QIcon::Normal);
    pauseIcon.addFile(":iconButtonPauseLocked",QSize(32,32),QIcon::Disabled);

    pauseButton = new QPushButton(groupBoxControl);
    pauseButton->setFlat(true);
    pauseButton->setIcon(QIcon(pauseIcon));
    pauseButton->setIconSize(QSize(32,32));
    pauseButton->setFixedSize(40,36);

    stopIcon.addFile(":iconButtonStop",QSize(32,32),QIcon::Normal);
    stopIcon.addFile(":iconButtonStopLocked",QSize(32,32),QIcon::Disabled);

    stopButton = new QPushButton(groupBoxControl);
    stopButton->setFlat(true);
    stopButton->setIcon(QIcon(stopIcon));
    stopButton->setIconSize(QSize(32,32));
    stopButton->setFixedSize(40,36);

    layoutControl = new QHBoxLayout(groupBoxControl);
    layoutControl->setContentsMargins(6,2,6,6);
    layoutControl->addWidget(openButton);
    layoutControl->addWidget(playButton);
    layoutControl->addWidget(pauseButton);
    layoutControl->addWidget(stopButton);

    groupBoxControl->setLayout(layoutControl);

    connect(openButton,SIGNAL(clicked()),this,SLOT(buttonOpenClicked()));
    connect(playButton,SIGNAL(clicked()),this,SLOT(buttonPlayClicked()));
    connect(pauseButton, SIGNAL(clicked()),this,SLOT(buttonPauseClicked()));
    connect(stopButton,SIGNAL(clicked()),this,SLOT(buttonStopClicked()));

    status=MidiFile::PlayerClosed;
    updateStatus();

    //SETTING INFO GROUPBOX
    groupBoxInfo = new QGroupBox("Song infos", this);

    fileNameLabel = new QLabel();
    fileNameLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Fixed);
    numTrackLabel = new QLabel();
    trackDurationLabel = new QLabel();

    layoutInfo = new QFormLayout();
    layoutInfo->setContentsMargins(6,2,6,6);
    layoutInfo->setVerticalSpacing(4);
    layoutInfo->addRow("File :", fileNameLabel);
    layoutInfo->addRow("Tracks :", numTrackLabel);
    layoutInfo->addRow("Duration :", trackDurationLabel);
    groupBoxInfo->setLayout(layoutInfo);

    scrollingTimer = new QTimer();
    scrollingTimer->setInterval(SCROLLING_INTERVAL);
    connect(scrollingTimer,SIGNAL(timeout()),this,SLOT(scrollingTick()));

    //SETTING MAIN LAYOUT
    layoutMain = new QHBoxLayout();
    layoutMain->addWidget(groupBoxControl);
    groupBoxControl->setFixedWidth(200);
    layoutMain->addWidget(groupBoxInfo);
    layoutMain->setMargin(0);
    this->setLayout(layoutMain);
}

void playControlWidget::buttonOpenClicked(void){
    filePath = QFileDialog::getOpenFileName(this, tr("Open File"),filePath,tr("Fichiers MIDI (*.mid)"));
    if(!filePath.isEmpty()){
        emit open(filePath);
    }
}

void playControlWidget::buttonPlayClicked(void){
    emit control(MidiFile::PlayerPlay);
}

void playControlWidget::buttonPauseClicked(void){
    emit control(MidiFile::PlayerPause);
}

void playControlWidget::buttonStopClicked(void){
    emit control(MidiFile::PlayerStop);
}

void playControlWidget::newStatus(MidiFile::PlayerState newStatus){
    status=newStatus;
    updateStatus();
}

void playControlWidget::updateStatus(void){
    switch(status){
        case MidiFile::PlayerClosed:
            openButton->setEnabled(true);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
            stopButton->setEnabled(false);
        break;
        case MidiFile::PlayerLocked:
            openButton->setEnabled(false);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
            stopButton->setEnabled(false);
        break;
        case MidiFile::PlayerStop:
            openButton->setEnabled(true);
            playButton->setEnabled(true);
            pauseButton->setEnabled(true);
            stopButton->setEnabled(false);
        break;
        case MidiFile::PlayerPause:
            openButton->setEnabled(true);
            playButton->setEnabled(true);
            pauseButton->setEnabled(false);
            stopButton->setEnabled(true);
        break;
        case MidiFile::PlayerPlay:
            openButton->setEnabled(true);
            playButton->setEnabled(false);
            pauseButton->setEnabled(true);
            stopButton->setEnabled(true);
        break;
    }

}

void playControlWidget::updateFileInfo(QString newFileName, quint16 numTracks, quint16 numMusicalTracks, QString newTrackDuration){

    trackDurationLabel->setText(newTrackDuration);
    fileName=newFileName;
    fileNameLabel->setText(fileName);
    numTrackLabel->setText(QString::number(numTracks)+" ("+QString::number(numMusicalTracks)+" audio)");
    if(fileNameLabel->fontMetrics().width(fileName)>fileNameLabel->width()){
        scrollCounter=0;
        scrollingTimer->start();
    }
    else{
        scrollingTimer->stop();
    }
}

void playControlWidget::scrollingTick(){
    if(scrollCounter<DELAY_BEFORE_SCROLLING){scrollCounter++;}
    else{
        QString fileNameLabelText=fileNameLabel->text();
        if(!fileNameLabelText.isEmpty()){
            fileNameLabel->setText(fileNameLabelText.remove(0,SCROLLING_NB_CHAR));
        }
        else{
            fileNameLabel->setText(fileName);
            scrollCounter=0;
        }
    }
}

void playControlWidget::resizeEvent(QResizeEvent *event){
    Q_UNUSED(event);
    if(fileNameLabel->fontMetrics().width(fileName)>fileNameLabel->width()){
        if(!scrollingTimer->isActive()){
            scrollCounter=0;
            scrollingTimer->start();
        }
    }
    else{
        scrollingTimer->stop();
        fileNameLabel->setText(fileName);
    }
}

QString playControlWidget::getLastOpenedFilePath(void){
    return filePath;
}

void playControlWidget::setLastOpenedFilePath(QString lastOpenedFilePath){
    filePath=lastOpenedFilePath;
}

playControlWidget::~playControlWidget(){
    delete scrollingTimer;
    delete playButton;
    delete pauseButton;
    delete stopButton;
    delete openButton;
    delete fileNameLabel;
    delete trackDurationLabel;
    delete numTrackLabel;
    delete layoutControl;
    delete layoutInfo;
    delete groupBoxControl;
    delete groupBoxInfo;
    delete layoutMain;
}
