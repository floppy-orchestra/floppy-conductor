#include "comlink.h"

ComLink::ComLink(QObject *parent) :
    QObject(parent)
{
    port = new QSerialPort(this);
    connect(port,SIGNAL(error(QSerialPort::SerialPortError)),this,SLOT(handleError(QSerialPort::SerialPortError)));
}

void ComLink::setSettings(ComLink::SerialSettings settings){
    port->close();

    port->setPortName(settings.portName);
    port->setBaudRate(settings.baudRate);
    port->setDataBits(settings.dataBits);
    port->setParity(settings.parity);
    port->setStopBits(settings.stopBits);
    port->setFlowControl(settings.flowControl);

    emit portSettingsChanged(getSettings());
    emit portStateChanged(port->isOpen());
    emit statusString(port->isOpen()?"<font color='Green'>Connected</font>":"<font color='Red'>Unconnected</font>");
}

void ComLink::togglePortState(void){
    if(port->isOpen()){
        port->close();
    }
    else{
        port->open(QIODevice::ReadWrite);
    }
    emit portStateChanged(port->isOpen());
    if(port->isOpen()){
        emit statusString("<font color='Green'>Connected</font>");
    }
    else if(port->error() == QSerialPort::DeviceNotFoundError){
        emit statusString("<font color='Red'>Invalid device selected</font>");
    }
    else{
        emit statusString("<font color='Red'>Unconnected</font>");
    }
}

void ComLink::writeByteArray(QByteArray dataToWrite){
    if(port->isOpen()){
        port->write(dataToWrite.data());
        emit statusString("<font color='Green'>Data transmitted : "+QByteArrayToQString(dataToWrite)+"</font>");
    }
}

void ComLink::handleError(QSerialPort::SerialPortError error){
    if (error == QSerialPort::ResourceError) {
        port->close();      //Needed because port is not automatically closed when device disconnected
        emit portStateChanged(port->isOpen());
        emit statusString("<font color='Red'>Device disconnected</font>");
    }
}

QString ComLink::QByteArrayToQString(QByteArray byteArray){
    QString stringToReturn;
    for(int i = 0;i<byteArray.size();i++){
        if(i>0){stringToReturn.append(" ");}
        stringToReturn.append(QString::number((quint8)byteArray.at(i),16).toUpper());
    }
    return stringToReturn;
}

ComLink::SerialSettings ComLink::getSettings(){
    SerialSettings settings;
    settings.portName = port->portName();
    settings.baudRate = port->baudRate();
    settings.dataBits = port->dataBits();
    settings.parity   = port->parity();
    settings.stopBits = port->stopBits();
    settings.flowControl = port->flowControl();
    return settings;
}
