#ifndef MIDITOCOMCONVERTER_H
#define MIDITOCOMCONVERTER_H

//#define GENERATE_NOTE_ARRAY

#include <QObject>
#include <QByteArray>
#include <QMap>
#include <QMultiHash>
#include <QStringList>
#include "midievent.h"
#include "midifile.h"

#ifdef GENERATE_NOTE_ARRAY
#include <QTime>
#include <QList>
#endif


class DummyFloppyDrive : public QObject
{
    Q_OBJECT
public:
    explicit DummyFloppyDrive(int newDriveNum);
    ~DummyFloppyDrive();

    int getDriveNum(void);
    bool getIsPlaying(void);
    uint getOriginalNote(void);
    uint getTransposedNote(void);
    void updateTransposedNote(int transposeValue);

    void play(uint newOriginalNote, int transposeValue=0);
    void resume(void);
    void pause(void);
    void stop(void);

signals:
    void comData(QByteArray data);

private:
    int driveNum;
    bool isPlaying;
    bool isPaused;
    uint originalNote;
    uint transposedNote;

    void sendOnCommand();
    void sendOffCommand();

#ifdef GENERATE_NOTE_ARRAY
public:
    struct noteReport{
        uint note;
        uint msDuration;
    };
    void appendReport(void);
    void printNoteReport(void);
private:
    QList<noteReport> noteList;
    QTime* noteTime;
#endif

};

//**************************************************************************************************************************************
//**************************************************************************************************************************************
//**************************************************************************************************************************************

class MidiToComConverter : public QObject
{
    Q_OBJECT
public:
    explicit MidiToComConverter(QObject *parent = 0);
    int getTransposeValue(void);
    QMap<int, QString> getDriveStringMap(void);

signals:
    void comData(QByteArray data);
    void driveMapUpdated(QMap<int,QString> driveMapString);

public slots:
    void onPlayerStateChanged(MidiFile::PlayerState state);
    void eventListToConvert(QList<MidiEvent> eventList);

    void setAvailableDrivesNum(uint availablesDrives);
    void transposeCommand(int newTransposeValue);
    void stopTrack(quint16 trackNum);

    void loadDriveMapFromSettings(QStringList mapStringList);
    void setDriveMapping(quint16 track, QString newMapString);

private:
    void processEvent(MidiEvent eventToProcess);

    int transposeValue;
    QList<DummyFloppyDrive*> dummyDrivesList;
    QMap<quint32,DummyFloppyDrive*> playingDrivesMap;
    QMultiMap<quint16, int> trackDrivesMap;
};




#endif // MIDITOCOMCONVERTER_H
