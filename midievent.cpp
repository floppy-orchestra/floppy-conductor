#include "midievent.h"

MidiEvent::MidiEvent(quint16 newTrackNum):nextEvent(NULL),deltaTime(0),param(0),type(other){
    trackNum=newTrackNum;
}

MidiEvent::MidiEvent(quint16 newTrackNum, quint32 newDeltaTime):nextEvent(NULL),param(0),type(other){
    deltaTime=newDeltaTime;
    trackNum=newTrackNum;
}

MidiEvent::MidiEvent(quint16 newTrackNum,quint32 newDeltaTime, MidiEventType newType, quint32 newParam):nextEvent(NULL){
    deltaTime=newDeltaTime;
    param=newParam;
    trackNum=newTrackNum;
    type=newType;
}

MidiEvent::MidiEvent(quint16 newTrackNum, MidiEventType newType):nextEvent(NULL),deltaTime(0),param(0){
    type=newType;
    trackNum=newTrackNum;
}

quint32 MidiEvent::getDeltaTime(void){
    return deltaTime;
}

MidiEvent* MidiEvent::getNextEvent(void){
    return nextEvent;
}

void MidiEvent::setNextEvent(MidiEvent* newNextEvent){
    nextEvent=newNextEvent;
}

MidiEvent::MidiEventType MidiEvent::getType(void){
    return type;
}

quint32 MidiEvent::getParam(void){
    return param;
}

quint16 MidiEvent::getTrackNum(void){
    return trackNum;
}

bool MidiEvent::isMusical(void){
    switch(type){
    case noteOn:
        return true;
    case noteOff:
        return true;
    default:
        return false;
    }
}

bool MidiEvent::isUseless(void){
    switch(type){
    case other:
        return true;
    case endOfTrack:
        return true;
    case beginTrack:
        return true;
    default:
        return false;
    }
}
